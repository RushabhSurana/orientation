### What is Docker?



An open-source project that automates the deployment of software applications inside containers by providing an additional layer of abstraction and automation of OS-level virtualization on Linux.

### Basic Docker Architecture

![Docker_Architecture](/uploads/f2ecd499d2bdeda0123b7eba11ae0160/Docker_Architecture.png)



### Some Docker Terminologies to remember


* **Docker Image** : An image is essentially built from the instructions for a complete and executable version of an application, which relies on the host OS kernel

* **Container** : A Docker container image is a lightweight, standalone, executable package of software that includes everything needed to run an application.

* **Docker Hub** : Docker Hub is a hosted repository service provided by Docker for finding and sharing container images with your team. Key features include: Private Repositories: Push and pull container images.

### Some Basic Docker Commands
`docker ps `: Allows us to view all the containers that are running on the Docker Host.

`docker start` : Starts any stopped container(s).

`docker stop` : Stops any running container(s).

`docker rm `: Deletes the containers


![Docker_Workflow](/uploads/668b43beb7b59ca5e07ca929f0eed365/Docker_Workflow.png)



### The reason we should use Docker??

For developers, there arises a situation where they need to work on different projects. And for each different project they might need a specific operating system, or a specific version of operating system. This becomes a tedious job to do. So problem can be solved using virtual machines, but this requires exact enviornment to run an application. Here we use Docker. The developer can work on the application and save it as docker image and create a docker container. This container has everything which is required to run an application alongwith the enviornment.This docker is then stored to repository and then can be used by other developers. So docker is used by many developers.

