# Git Summary

**What is Git**

Git is version-control software that makes collaboration with teammates super simple and it lets you easily keep track of every revision you and your team make during the development of your software.It is designed for coordinating work among programmers, but it can be used to track changes in any set of files. Its goals include speed, data integrity, and support for distributed, nonlinear workflows.


![Git_Workflow](/uploads/6920e945471dc41864af675e69109b97/Git_Workflow.png)


## Required Vocabury
* **Repository** is the collection of files and folders that you’re using git to track.

* **Commit** saves your work in thee Repository.

* **Push** means syncing your commits.

* **Branch** are the separate instance of the main code.

* **Merge** means integrating the branch with main code.

* **Clone** means making a copy of main code into local machine

* **Fork** means making a copy of main code in your Repositorywith ypur username.


### How to Get Started with GIT

##### How to install Git

#### For Linux: sudo apt-get install git
#### For Windows: Download the git installer and run it

### Filling some basic details first:
`git config --global user.name "Your Name Comes Here"`

`git config --global user.email your_email@yourdomain.example.com`


##### Starting a new project on git
In order to start a new project on git we use the linux command mkdir(make directory) to create a new directory to store data of our project using the command given below

`mkdir project_name`

In order to access a project that already exists we use the cd command where cd means change directory as shown below

`cd project_name`

In order to delete a project file we use the linux command rmdir (remove directory) to delete the directory permanently

`rmdir project_name`

![Git](/uploads/adc0a229514719e58824e97ab4f10ed6/Git.png)

### Git workflow

* **Clone repo** : `$ git clone (link to repository)`

* **Create new branch** : ` $ git checkout master` or `$ git checkout -b (your branch name)`

* **Staging changes** : `$ git add `.

* **Commit changes** : `$ git commit -sv`

* **Push Files** : `$ git push origin (branch name)`

![git_basics](/uploads/ba9f8252acaa0b3f42ad4aaeaef27388/git_basics.png)










